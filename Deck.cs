﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoDecks
{
    class Deck
    {
        private List<Card> cards;
        private Random random = new Random();

        public Deck()
        {
            cards = new List<Card>();
            for (int suit = 0; suit <= 3; suit++)
                for (int value1 = 1; value1 <= 4; value1++)
                    cards.Add(new Card((Suit)suit, (value)value1));
        }

        public Deck(IEnumerable<Card> initialCards)
        {
            cards = new List<Card>(initialCards);
        }

        public int Count { get { return cards.Count; } }

        public void Add(Card cardToAdd)
        {
            cards.Add(cardToAdd);
        }

        public Card Deal(int index)
        {
            Card cardToDeal = cards[index];
            cards.RemoveAt(index);
            return cardToDeal;
        }

        public void Shuffle()
        {
            cards.Sort(new CardShuffler());
        }

        public IEnumerable<string> GetCardNames()
        {
            //I'm having some confusion as to what IEnumerable<string> does.  I have a vague notion it's a List object but is somewhat related to a String[];
            //My confusion started when I noticed I was able to return the string array "temp" in this method even though the return type is IEnumerable<string>
            //At one point I tried the following code:  string[] tempArray = deck1.GetCardNames();   but it said my types were incorrect.  
            //However by upcasting I was able to make the code work:  IEnumerable<string> upcastTest = deck1.GetCardNames();  string[] tempArray = upcastTest as string[];
          
            String[] temp = new string[cards.Count];
            for (int i = 0; i < cards.Count; i++)
                    temp[i] = cards[i].ToString();         
            return temp;
        }

        public void Sort()
        {
            cards.Sort(new CardComparer_bySuit());
        }

    }
}

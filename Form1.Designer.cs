﻿namespace TwoDecks
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.ToDeck2 = new System.Windows.Forms.Button();
            this.ToDeck1 = new System.Windows.Forms.Button();
            this.Reset2 = new System.Windows.Forms.Button();
            this.shuffle2 = new System.Windows.Forms.Button();
            this.Reset1 = new System.Windows.Forms.Button();
            this.shuffle1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(307, 60);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(190, 384);
            this.listBox1.TabIndex = 0;
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 20;
            this.listBox2.Location = new System.Drawing.Point(13, 60);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(194, 384);
            this.listBox2.TabIndex = 1;
            // 
            // ToDeck2
            // 
            this.ToDeck2.Location = new System.Drawing.Point(221, 110);
            this.ToDeck2.Name = "ToDeck2";
            this.ToDeck2.Size = new System.Drawing.Size(75, 35);
            this.ToDeck2.TabIndex = 2;
            this.ToDeck2.Text = ">>";
            this.ToDeck2.UseVisualStyleBackColor = true;
            this.ToDeck2.Click += new System.EventHandler(this.ToDeck2_Click);
            // 
            // ToDeck1
            // 
            this.ToDeck1.Location = new System.Drawing.Point(221, 165);
            this.ToDeck1.Name = "ToDeck1";
            this.ToDeck1.Size = new System.Drawing.Size(75, 36);
            this.ToDeck1.TabIndex = 3;
            this.ToDeck1.Text = "<<";
            this.ToDeck1.UseVisualStyleBackColor = true;
            this.ToDeck1.Click += new System.EventHandler(this.ToDeck1_Click);
            // 
            // Reset2
            // 
            this.Reset2.Location = new System.Drawing.Point(307, 462);
            this.Reset2.Name = "Reset2";
            this.Reset2.Size = new System.Drawing.Size(190, 34);
            this.Reset2.TabIndex = 4;
            this.Reset2.Text = "Reset Deck #2";
            this.Reset2.UseVisualStyleBackColor = true;
            this.Reset2.Click += new System.EventHandler(this.Reset2_Click);
            // 
            // shuffle2
            // 
            this.shuffle2.Location = new System.Drawing.Point(307, 503);
            this.shuffle2.Name = "shuffle2";
            this.shuffle2.Size = new System.Drawing.Size(190, 35);
            this.shuffle2.TabIndex = 5;
            this.shuffle2.Text = "Shuffle Deck #2";
            this.shuffle2.UseVisualStyleBackColor = true;
            this.shuffle2.Click += new System.EventHandler(this.shuffle2_Click);
            // 
            // Reset1
            // 
            this.Reset1.Location = new System.Drawing.Point(13, 462);
            this.Reset1.Name = "Reset1";
            this.Reset1.Size = new System.Drawing.Size(194, 34);
            this.Reset1.TabIndex = 6;
            this.Reset1.Text = "Reset Deck #1";
            this.Reset1.UseVisualStyleBackColor = true;
            this.Reset1.Click += new System.EventHandler(this.Reset1_Click);
            // 
            // shuffle1
            // 
            this.shuffle1.Location = new System.Drawing.Point(13, 503);
            this.shuffle1.Name = "shuffle1";
            this.shuffle1.Size = new System.Drawing.Size(194, 35);
            this.shuffle1.TabIndex = 7;
            this.shuffle1.Text = "Shuffle Deck 1";
            this.shuffle1.UseVisualStyleBackColor = true;
            this.shuffle1.Click += new System.EventHandler(this.shuffle1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(307, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "label2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 541);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.shuffle1);
            this.Controls.Add(this.Reset1);
            this.Controls.Add(this.shuffle2);
            this.Controls.Add(this.Reset2);
            this.Controls.Add(this.ToDeck1);
            this.Controls.Add(this.ToDeck2);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.listBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Button ToDeck2;
        private System.Windows.Forms.Button ToDeck1;
        private System.Windows.Forms.Button Reset2;
        private System.Windows.Forms.Button shuffle2;
        private System.Windows.Forms.Button Reset1;
        private System.Windows.Forms.Button shuffle1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoDecks
{
    class Card
    {
        public TwoDecks.Suit suit;
        public value value;

        public Card(TwoDecks.Suit suit, value value)
        {
            this.suit = suit;
            this.value = value;
        }

        public override string ToString()
        {
            return this.value + " of " + this.suit;
        }

    }
}

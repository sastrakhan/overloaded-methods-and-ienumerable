﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoDecks
{
    class CardComparer_bySuit : IComparer<Card>
    {
        public int Compare(Card x, Card y)
        {
            if (x.value < y.value)
                return -1;
            if (x.value > y.value)
                return 1;
            if (x.suit < y.suit)
                return -1;
            if (x.suit > y.suit)
                return 1;
            return 0;
            throw new NotImplementedException();
        }
    }
}
